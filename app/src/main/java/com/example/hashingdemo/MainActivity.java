package com.example.hashingdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.mindrot.jbcrypt.BCrypt;


public class MainActivity extends AppCompatActivity {

    private static final String PASSWORD = "password123";
    private static final String SALT = "password123";
    private String PASSWORD_HASH = getPasswordHash(PASSWORD);

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.tv);
    }

    public String getPasswordHash(String password) {
        String pw_hash = BCrypt.hashpw(password, BCrypt.gensalt());
        return pw_hash;
    }

    public void verify(View view) {
        String password = ((EditText)(findViewById(R.id.edittext))).getText().toString();

//        textView.setText(PASSWORD_HASH+"\n\n"+getPasswordHash(password));

        try {
            if (HashingWithPBKDF2.generateStorngPasswordHash("password123123").equals(HashingWithPBKDF2.generateStorngPasswordHash("password123123")))
                showToast("It matches");
            else
                Toast.makeText(this, "It does not matches", Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            showToast("exception");
        }
       /* if (BCrypt.checkpw(password, PASSWORD_HASH))
            showToast("It matches");
        else
            Toast.makeText(this,"It does not matches",Toast.LENGTH_SHORT).show();*/
    }

    private void showToast(String info) {
        Toast.makeText(this,info,Toast.LENGTH_SHORT).show();
    }

}
